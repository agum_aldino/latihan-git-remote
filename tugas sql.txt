Soal Berlatih SQL:

Soal 1 Membuat Database:
    create database myshop;


Soal 2 Membuat Table di dalam Database:
Table users
    create table genre( id int(8) primary key auto_increment, name varchar(255), email varchar(255), password varchar(255));

Table categories
    create table categories( id int(8) primary key auto_increment, name varchar(255));

Table items
    create table items( id int(8) primary key auto_increment, name varchar(255), description varchar(255), price int(8), stock int(8), category_id int (8), foreign key(category_id) references categories(id));


Soal 3 Memasukkan Data pada Table
Table users
    insert into users(name, email, password) values("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

Table categories
    insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

Table items
    insert into items(name, description, price, stock, category_id) values("Summsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Unikloah", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


Soal 4 Mengambil Data dari Database
a. mengambil data kecuali password di table users
    select name, email from users;

b. mengambil data items    
- mengambil data item dengan kondisi harga > 1000000
    select *from items where harga>1000000;

- mengambil data menggunakan Like
    select *from items where judul like '%sang%';

- mengambil data di kedua table mengggunakan join
    select items.nama, items.description, items.price, items.stock, items.category_id, categories.name from items inner join cetegories on items.category_id = categories.id;


Soal 5 Mengubah Data dari Database
    update items set price=2.500.000 where id=1;
